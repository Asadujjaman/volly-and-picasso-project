package com.example.vollyandpicasso;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.example.vollyandpicasso.MainActivity.EXTRA_CREATOR;
import static com.example.vollyandpicasso.MainActivity.EXTRA_LIKE;
import static com.example.vollyandpicasso.MainActivity.EXTRA_URL;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra(EXTRA_URL);
        String creatorName = intent.getStringExtra(EXTRA_CREATOR);
        int likesCount = intent.getIntExtra(EXTRA_LIKE,0);

        ImageView imageView = findViewById(R.id.image_view_detail);
        TextView textViewCreator = findViewById(R.id.text_view_details);
        TextView textViewLikes = findViewById(R.id.text_view_likes_details);

        Picasso.with(this).load(imageUrl).fit().centerInside().into(imageView);
        textViewCreator.setText(creatorName);
        textViewLikes.setText("Likes: "+likesCount);
    }
}
